"""
   author: 陈春雨
   date: 2021/6/15

   一个弹簧模拟游戏
"""

import pygame, sys
import numpy as np
from mySpring import Springs

pygame.init()

t=0.0001 # 步长
l = 65 # 弹簧长度
g = np.array([0, 800]) # 重力加速度
k = 3000 # 弹簧弹性
damp = 1000

sp = Springs(l, k, g, t, damp) # 定义一个弹簧系统

#一些基本设置
size = width, heigth = (800, 800) # 屏幕大小
black = (0, 0, 0) # 背景颜色

white = (255, 255, 255)
blue = (0, 199, 140)
red = (244, 96, 108)
red2 = (236, 173, 158)
grey = (128, 128, 105)
blue2 = (8, 46, 84)
red3 = (255, 127, 80)

#设置屏幕大小
screen = pygame.display.set_mode(size, pygame.RESIZABLE)

#设置标题
pygame.display.set_caption("弹簧小游戏")

#导入素材
icon = pygame.image.load("kkk.jpg")

#设置游戏图标
pygame.display.set_icon(icon)

fps = 100
fclock = pygame.time.Clock()

def updata_speed(F):
    F = F+np.array([0, g])
    speed = speed+F

while True:
    #获取事件
    for event in pygame.event.get():
        #如果事件是退出, 就将游戏关闭
        if event.type == pygame.QUIT:
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_c:
               sp.clean()
            if event.key == pygame.K_ESCAPE:
               sys.exit()
            if event.key == pygame.K_w:
               sp.k *= 1.1
            if event.key == pygame.K_s:
               sp.k /= 1.1

        elif event.type == pygame.MOUSEBUTTONDOWN:
                sp.insert_point(event.pos[0], event.pos[1])

        elif event.type == pygame.VIDEORESIZE:
            size = width, heigth = event.w, event.h
            screen = pygame.display.set_mode(size, pygame.RESIZABLE)
            pygame.display.update()


    #将游戏的数据更新显示在屏幕上
    loc = sp.location
    speed = sp.speed

    #flag = loc[:, 0] < 0
    #loc[flag, 0] = 0
    #speed[flag, 0] *= -1

    #flag = loc[:, 1] < 0
    #loc[flag, 1] = 1
    #speed[flag, 1] *= -1

    #flag = loc[:, 0] > width
    #loc[flag, 0] = width
    #speed[flag, 0] *= -1

    flag = loc[:, 1] > heigth
    loc[flag, 1] = heigth
    speed[flag] = 0

    screen.fill(blue)
    sp.updata_speed()
    sp.updata_locastion()
    for i in range(len(sp.location)):
        pygame.draw.circle(screen, red3, sp.location[i], 4, 0)
    for i in range(len(sp.top)):
        pygame.draw.aaline(screen, 0x0, sp.location[sp.top[i, 0]], 
                sp.location[sp.top[i, 1]], 3)
    pygame.display.update()
    fclock.tick(fps)

