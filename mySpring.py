"""
    author: 陈春雨
    date: 2021/6/15

    一个描述弹簧的类
"""

import sys
import numpy as np

class Springs:
    def __init__(self, l, k, g, t, damp):
        self.location = np.array([[300, 500], [330, 440], [270, 440]], dtype=np.int_)#质点的坐标
        self.top = np.array([[0, 1], [0, 2], [1, 2]], dtype=np.int_) #质点的连接关系
        self.speed = np.zeros([3, 2], dtype=np.float64) 

        self.l = l # 弹簧长度
        self.k = k # 弹簧弹性
        self.g = g # 重力加速度
        self.t = t # 每两次刷新时间间隔
        self.damp = damp

    def insert_point(self, x, y):
        n = len(self.location)
        newpoint = np.array([[x, y]])
        
        flag = np.all(np.abs(self.location - newpoint) < self.l*1.4, axis=1)

        idx = np.where(flag)[0]

        self.top = np.r_[self.top, np.c_[idx.reshape(-1, 1) , 
            n*np.ones([len(idx), 1],dtype=np.int_)] ]

        self.location = np.r_[self.location, newpoint]
        self.speed = np.r_[self.speed, np.zeros([1, 2], dtype=np.int_)]

    def computer_force_of_points(self):
        top = self.top
        loc = self.location
        NE = len(top)
        NP = len(loc)

        v = (loc[top[:, 1]] - loc[top[:, 0]]).astype(np.float64)
        l = np.linalg.norm(v, axis=1)
        if np.any(l>1000):
            sys.exit()

        v = (v.T/l).T
        f = (self.k*(l-self.l)*(v.T)).T
        F = np.zeros([NP, 2], dtype=np.float64)
        np.add.at(F, top[:, 0], f)
        np.add.at(F, top[:, 1], -f)
        return F

    def updata_speed(self):
        F = self.computer_force_of_points()
        self.speed = self.speed * np.exp(-self.t * self.damp)
        self.speed = self.speed + (F + self.g)/1
        
    def updata_locastion(self):
        self.location = self.location + self.speed*self.t

    def clean(self):
        self.speed = np.zeros([3, 2], dtype=np.float64) 
        self.location = np.array([[300, 500], [310, 490], [290, 490]], dtype=np.int_)#质点的坐标
        self.top = np.array([[0, 1], [0, 2], [1, 2]], dtype=np.int_) #质点的连接关系


