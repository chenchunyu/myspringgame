
作者: 陈春雨

这是一个弹簧模拟的小游戏.

昨天看了胡渊明博士在 bilibili 上发布的 games201 的第二节课, 胡博士展示了一个这样的小游戏, 用到了
常微分方程数值解方法, 原理很简单, 我就也动手写了一个, 用 python 写的, 用的 pygame 的游戏框架, 所以需要电脑中
安装了 numpy 和 pygame.

根据这个小例子, 说明游戏制作距离数学也没有很远：）

#操作: 在游戏界面点点点就可以用了 ：）

#安装
pip3 install numpy
pip3 install pygame

#运行
python3 mySpringGame.py


